#include <epicsEvent.h>
#include <epicsTime.h>
#include "asynNDArrayDriver.h"

/* Definition of ps4000 driver routines on Linux */
#include <libps4000-1.2/ps4000Api.h>
#ifndef PICO_STATUS
#include <libps4000-1.2/PicoStatus.h>
#endif

#define DRIVER_VERSION      2
#define DRIVER_REVISION     0
#define DRIVER_MODIFICATION 0

// unit parameters
#define PS4000StatusString             "PS4000_STATUS"
#define PS4000StatusMessageString      "PS4000_STATUS_MESSAGE"
#define PS4000TimeBaseString           "PS4000_TIME_BASE"
#define PS4000TimeIntervalString       "PS4000_TIME_INTERVAL"
#define PS4000NumSamplesString         "PS4000_NUM_SAMPLES"
#define PS4000ExtTrigEnabledString     "PS4000_EXT_TRIG_ENABLED"
#define PS4000ExtTrigRangeString       "PS4000_EXT_TRIG_RANGE"
#define PS4000ExtTrigThrString         "PS4000_EXT_TRIG_THR"
#define PS4000ExtTrigThrDirString      "PS4000_EXT_TRIG_THR_DIR"
// channel parameters
#define PS4000ChNameString             "PS4000_CH_NAME"
#define PS4000ChEnabledString          "PS4000_CH_ENABLED"
#define PS4000ChCouplingString         "PS4000_CH_COUPLING"
#define PS4000ChRangeString            "PS4000_CH_RANGE"
#define PS4000ChConvertString          "PS4000_CH_CONVERT"

#define MAX_CHANNELS                 4
#define DUAL_SCOPE                   2
#define TRIPLE_SCOPE                 3
#define QUAD_SCOPE                   4

typedef enum
{
    MODEL_NONE = 0,
    MODEL_PS4223 = 4223,
    MODEL_PS4224 = 4224,
    MODEL_PS4423 = 4423,
    MODEL_PS4424 = 4424,
    MODEL_PS4226 = 4226,
    MODEL_PS4227 = 4227,
    MODEL_PS4262 = 4262
} MODEL_TYPE;

#define asynPrintError(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_ERROR, \
            "%s::%s [ERROR] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDeviceInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DEVICE, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintDriverInfo(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, \
            "%s::%s [INFO] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

#define asynPrintFlow(pasynUser, fmt, ...) { \
        asynPrint(pasynUser, ASYN_TRACE_FLOW, \
            "%s::%s [FLOW] " fmt "\n", \
            driverName, __func__, ##__VA_ARGS__); \
}

/** PicoScope PS4000 driver. */
class epicsShareClass PS4000 : public asynNDArrayDriver {
public:
    PS4000(const char *portName, int numTimePoints, NDDataType_t dataType,
                   int maxBuffers, size_t maxMemory,
                   int priority, int stackSize);
    virtual ~PS4000();

    /* These are the methods that we override from asynNDArrayDriver */
    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
    virtual void report(FILE *fp, int details);
    void acqTask(); /**< Should be private, but gets called from C, so must be public */

protected:
    int P_Status;
    #define FIRST_PS4000_PARAM P_Status
    int P_StatusMessage;
    int P_TimeBase;
    int P_TimeInterval;
    int P_NumSamples;
    int P_ExtTrigEnabled;
    int P_ExtTrigRange;
    int P_ExtTrigThr;
    int P_ExtTrigThrDir;
    int P_ChName;
    int P_ChEnabled;
    int P_ChCoupling;
    int P_ChRange;
    int P_ChConvert;

private:
    /* These are the methods that are new to this class */
    void setAcquire(int value);
    void getInfo();
    int16_t mvToAdc(int16_t mv, int16_t ch);
    int32_t adcToMv(int32_t raw, int32_t ch);
    asynStatus setupBuffers();
    asynStatus setupChannel(int ch);
    asynStatus setupExternalTrigger();
    asynStatus setupTimeBase();
    asynStatus setupDevice();
    asynStatus checkForDevice();
    template <typename epicsType> asynStatus updateArraysT();
    asynStatus updateArrays();

    /* Our data */
    epicsEventId mStartEventId;
    epicsEventId mStopEventId;
    int mUniqueId;
    int mAcquiring;

    int16_t mHandle;
    int16_t *mBuffer[MAX_CHANNELS];
    size_t mBufferSize;
    MODEL_TYPE mModel;
    PS4000_RANGE mFirstRange;
    PS4000_RANGE mLastRange;
    bool mHasSignalGenerator;
    bool mHasETS;
    int mChannelCount;
    size_t mSampleCount;

public:
    bool mDataReady;
};

