/* ps4000.cpp
 *
 * This is a driver for a PicoScope 4000 Series area detector.
 *
 * Author: Hinko Kocevar
 *         European Spallation Source ERIC
 *
 * Created:  December 19, 2017
 *
 * Based on ps4000con.[ch] from picosdk-c-examples:
 * Copyright (C) 2009 - 2017 Pico Technology Ltd. See LICENSE.md file for terms.
 *
 * EPICS areaDetector driver:
 * Copyright (C) 2018 - 2019 European Spallation Source ERIC
 */

#include <stddef.h>
#include <stdlib.h>
#include <stdarg.h>

#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>

#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsEvent.h>
#include <epicsMutex.h>
#include <epicsString.h>
#include <epicsStdio.h>
#include <epicsExport.h>
#include <epicsExit.h>
#include <cantProceed.h>
#include <iocsh.h>

#include "asynNDArrayDriver.h"

#include "ps4000.h"

static const char *driverName = "PS4000";

#define PREF4

uint16_t g_inputRanges [PS4000_MAX_RANGES] = {
                                                10,
                                                20,
                                                50,
                                                100,
                                                200,
                                                500,
                                                1000,
                                                2000,
                                                5000,
                                                10000,
                                                20000,
                                                50000};

static void acqTaskC(void *drvPvt) {
    PS4000 *pPvt = (PS4000 *)drvPvt;
    pPvt->acqTask();
}

/**
 * Exit handler, delete the SIS8300 object.
 */
static void exitHandler(void *drvPvt) {
    PS4000 *pPvt = (PS4000 *) drvPvt;
    delete pPvt;
}


void PS4000::getInfo() {
    int8_t description [11][25]= { "Driver Version",
                                    "USB Version",
                                    "Hardware Version",
                                    "Variant Info",
                                    "Serial",
                                    "Cal Date",
                                    "Kernel",
                                    "Digital H/W",
                                    "Analogue H/W",
                                    "Firmware 1",
                                    "Firmware 2"};

    int16_t i, r = 0;
    int8_t line[80];
    char firmware[80];
    int32_t variant = MODEL_NONE;
    PICO_STATUS status = PICO_OK;

    memset(firmware, 0, sizeof(firmware));
    if (mHandle)
    {
        for (i = 0; i < 11; i++)
        {
            status = ps4000GetUnitInfo(mHandle, line, sizeof(line), &r, i);
            if (status == PICO_INVALID_INFO) {
                continue;
            } else if (status) {
                asynPrintError(pasynUserSelf, "failed to get status: %d", status);
                return;
            }

            printf("%s: %s\n", description[i], line);

            switch (i) {
            case PICO_DRIVER_VERSION:
                setStringParam(ADSDKVersion, (char *)line);
                break;
            case PICO_USB_VERSION:
                break;
            case PICO_HARDWARE_VERSION:
                // setStringParam(P_Hardware, (char *)line);
                break;
            case PICO_VARIANT_INFO:
                setStringParam(ADModel, (char *)line);
                variant = atoi((char *)line);
                break;
            case PICO_BATCH_AND_SERIAL:
                setStringParam(ADSerialNumber, (char *)line);
                break;
            case PICO_CAL_DATE:
                break;
            case PICO_KERNEL_VERSION:
                break;
            case PICO_DIGITAL_HARDWARE_VERSION:
            case PICO_ANALOGUE_HARDWARE_VERSION:
                // ignore Analog and Digital H/W
                break;
            case PICO_FIRMWARE_VERSION_1:
                sprintf(firmware, "%s", (char *)line);
                break;
            case PICO_FIRMWARE_VERSION_2:
                r = strlen(firmware);
                sprintf(firmware+r, ", %s", (char *)line);
                setStringParam(ADFirmwareVersion, firmware);
                break;
            case PICO_MAC_ADDRESS:
                break;
            case PICO_SHADOW_CAL:
                break;
            case PICO_IPP_VERSION:
                break;
            case PICO_DRIVER_PATH:
                break;
            default:
                break;
            }
        }

        switch (variant)
        {
        case MODEL_PS4223:
            mModel           = MODEL_PS4223;
            mHasSignalGenerator = false;
            mHasETS             = false;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_50V;
            mChannelCount    = DUAL_SCOPE;
            break;

        case MODEL_PS4224:
            mModel           = MODEL_PS4224;
            mHasSignalGenerator = false;
            mHasETS             = false;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_20V;
            mChannelCount    = DUAL_SCOPE;
            break;

        case MODEL_PS4423:
            mModel           = MODEL_PS4423;
            mHasSignalGenerator = false;
            mHasETS             = false;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_50V;
            mChannelCount    = QUAD_SCOPE;
            break;

        case MODEL_PS4424:
            mModel           = MODEL_PS4424;
            mHasSignalGenerator = false;
            mHasETS             = false;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_20V;
            mChannelCount    = QUAD_SCOPE;
            break;

        case MODEL_PS4226:
            mModel           = MODEL_PS4226;
            mHasSignalGenerator = true;
            mHasETS             = true;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_20V;
            mChannelCount    = DUAL_SCOPE;
            break;

        case MODEL_PS4227:
            mModel           = MODEL_PS4227;
            mHasSignalGenerator = true;
            mHasETS             = true;
            mFirstRange      = PS4000_50MV;
            mLastRange       = PS4000_20V;
            mChannelCount    = DUAL_SCOPE;
            break;

        case MODEL_PS4262:
            mModel           = MODEL_PS4262;
            mHasSignalGenerator = true;
            mHasETS             = false;
            mFirstRange      = PS4000_10MV;
            mLastRange       = PS4000_20V;
            mChannelCount    = DUAL_SCOPE;
            break;

        default:
            break;
        }
    }
}

/****************************************************************************
* Callback
* used by ps4000 data block collection calls, on receipt of data.
* used to set global flags etc checked by user routines
****************************************************************************/
void PREF4 CallBackBlock(int16_t handle, PICO_STATUS status, void * pParameter) {
    PS4000 *pPvt = (PS4000 *)pParameter;

    // flag to say done reading data
    pPvt->mDataReady = true;
}

/****************************************************************************
* adc_to_mv
*
* Convert an 16-bit ADC count into millivolts
****************************************************************************/
int32_t PS4000::adcToMv(int32_t raw, int32_t ch) {
    return (raw * g_inputRanges[ch]) / PS4000_MAX_VALUE;
}

/****************************************************************************
* mv_to_adc
*
* Convert a millivolt value into a 16-bit ADC count
*
*  (useful for setting trigger thresholds)
****************************************************************************/
int16_t PS4000::mvToAdc(int16_t mv, int16_t ch) {
    return ((mv * PS4000_MAX_VALUE ) / g_inputRanges[ch]);
}


void PS4000::setAcquire(int value)
{
    if (value && !mAcquiring) {
        /* Send an event to wake up the simulation task */
        epicsEventSignal(this->mStartEventId);
    }
    if (!value && mAcquiring) {
        /* This was a command to stop acquisition */
        /* Send the stop event */
        epicsEventSignal(this->mStopEventId);
    }
}

asynStatus PS4000::checkForDevice() {
    PICO_STATUS status;

    // if there is a device handle use it
    if (mHandle > 0) {

        status = ps4000PingUnit(mHandle);
        if (status) {
            asynPrintError(pasynUserSelf, "ps4000PingUnit() returned %d", status);
            ps4000Stop(mHandle);
            ps4000CloseUnit(mHandle);
            mHandle = 0;
            mBufferSize = 0;
            setIntegerParam(P_Status, 9);
            setStringParam(P_StatusMessage, "Not connected!");
            // fall through and try to re-open the device
        } else {
            setIntegerParam(P_Status, 0);
            setStringParam(P_StatusMessage, "Connected");
            return asynSuccess;
        }
    }

    assert(mHandle <= 0);

    asynPrintDriverInfo(pasynUserSelf, "opening the device..");
    status = ps4000OpenUnit(&mHandle);
    asynPrintDriverInfo(pasynUserSelf, "ps4000OpenUnit() returned %d", status);
    if (status != PICO_OK && status != PICO_EEPROM_CORRUPT) {
        mBufferSize = 0;
        setIntegerParam(P_Status, 9);
        setStringParam(P_StatusMessage, "Not connected!");
        return asynError;
    }
    asynPrintDriverInfo(pasynUserSelf, "device opened successfully!");

    // get device info
    getInfo();

    if(mHasETS) {
        // turn off ETS
        status = ps4000SetEts(mHandle, PS4000_ETS_OFF, 0, 0, NULL);
        asynPrintDriverInfo(pasynUserSelf, "ps4000SetEts() returned %d", status);
    }

    setIntegerParam(P_Status, 0);
    setStringParam(P_StatusMessage, "Connected");
    return asynSuccess;
}


/** Template function to compute the simulated detector data for any data type */
template <typename epicsType> asynStatus PS4000::updateArraysT() {
    int numSamples;
    unsigned int gotSamples;
    int timeBase;
    NDDataType_t dataType;
    epicsType *pData;
    PICO_STATUS status;

    getIntegerParam(NDDataType, (int *)&dataType);
    getIntegerParam(P_NumSamples, &numSamples);
    getIntegerParam(P_TimeBase, &timeBase);

    /* Start it collecting, then wait for completion*/
    mDataReady = false;
    status = ps4000RunBlock(mHandle, 0, numSamples, (unsigned int)timeBase,
            1,    NULL, 0, CallBackBlock, (void *)this);
    asynPrintDriverInfo(pasynUserSelf, "ps4000RunBlock() returned %d", status);
    if (status) {
        return asynError;
    }

    asynPrintDriverInfo(pasynUserSelf, "waiting for trigger..");
    while (! mDataReady) {
        usleep(1000);

        /* Has acquisition been stopped? */
        status = epicsEventTryWait(this->mStopEventId);
        if (status == epicsEventWaitOK) {
            mAcquiring = 0;
        }

        if (! mAcquiring) {
            ps4000Stop(mHandle);
            asynPrintError(pasynUserSelf, "trigger did not arrive!");
            return asynTimeout;
        }
    }

    if (! mDataReady) {
        ps4000Stop(mHandle);
        asynPrintError(pasynUserSelf, "data did not arrive!");
        return asynTimeout;
    }

    status = ps4000GetValues(mHandle, 0, (uint32_t*) &gotSamples, 1, RATIO_MODE_NONE, 0, NULL);
    asynPrintDriverInfo(pasynUserSelf, "ps4000GetValues() returned %d", status);
    if (status) {
        return asynError;
    }
    asynPrintDriverInfo(pasynUserSelf, "we got %d samples (wanted %d)", gotSamples, numSamples);

    // each trace in its own array
    size_t dims[1];
    dims[0] = gotSamples;

    for (int ch = 0; ch < mChannelCount; ch++) {
        int enabled;
        int convert;
        int range;
        getIntegerParam(ch, P_ChEnabled, &enabled);
        if (enabled) {
            if (this->pArrays[ch]) {
                this->pArrays[ch]->release();
            }
            this->pArrays[ch] = pNDArrayPool->alloc(1, dims, dataType, 0, 0);
            pData = (epicsType *)this->pArrays[ch]->pData;
            memset(pData, 0, numSamples * sizeof(epicsType));

            getIntegerParam(ch, P_ChConvert, &convert);
            getIntegerParam(ch, P_ChRange, &range);

            for (unsigned int o = 0; o < gotSamples; o++) {
                if (convert) {
                    pData[o] = (epicsType) adcToMv(mBuffer[ch][o], range);
                } else {
                    pData[o] = (epicsType) mBuffer[ch][o];
                }
            }
        }
    }

    return asynSuccess;
}

/** Get the new scope data */
asynStatus PS4000::updateArrays() {
    int dataType;
    getIntegerParam(NDDataType, &dataType);

    switch (dataType) {
        case NDInt8:
            return updateArraysT<epicsInt8>();
            break;
        case NDUInt8:
            return updateArraysT<epicsUInt8>();
            break;
        case NDInt16:
            return updateArraysT<epicsInt16>();
            break;
        case NDUInt16:
            return updateArraysT<epicsUInt16>();
            break;
        case NDInt32:
            return updateArraysT<epicsInt32>();
            break;
        case NDUInt32:
            return updateArraysT<epicsUInt32>();
            break;
        case NDFloat32:
            return updateArraysT<epicsFloat32>();
            break;
        case NDFloat64:
            return updateArraysT<epicsFloat64>();
            break;
    }

    return asynError;
}

asynStatus PS4000::setupDevice() {
    asynStatus status = asynSuccess;

    status = checkForDevice();
    if (status) {
        return status;
    }

    status = setupBuffers();
    if (status) {
        return status;
    }

    for (int i = 0; i < mChannelCount; i++) {
        status = setupChannel(i);
        if (status) {
            return status;
        }
    }

    status = setupExternalTrigger();
    if (status) {
        return status;
    }

    status = setupTimeBase();
    if (status == asynSuccess) {
        setIntegerParam(P_Status, 0);
        setStringParam(P_StatusMessage, "Idle");
    }

    return status;
}

void PS4000::acqTask()
{
    int status = asynSuccess;
    NDArray *pTrace;
    epicsTimeStamp startTime;
    int arrayCounter;
    int i;
    int extTrigEnabled;
    int chEnabled;
    int numSamples;
    const char *functionName = "acqTask";

    this->lock();
    /* Loop forever */
    while (1) {
        /* Has acquisition been stopped? */
        status = epicsEventTryWait(this->mStopEventId);
        if (status == epicsEventWaitOK) {
            mAcquiring = 0;
        }

        /* If we are not acquiring then wait for a semaphore that is given when acquisition is started */
        if (!mAcquiring) {
          /* Release the lock while we wait for an event that says acquire has started, then lock again */
            asynPrint(this->pasynUserSelf, ASYN_TRACE_FLOW,
                "%s:%s: waiting for acquire to start\n", driverName, functionName);
            callParamCallbacks();

            this->unlock();
            status = epicsEventWait(this->mStartEventId);
            this->lock();
            mAcquiring = 1;

            // force call to setupDevice() below
            mSampleCount = 0;
        }

        // see if the device is present, try to connect if not
        status = checkForDevice();
        if (status) {
            setIntegerParam(ADAcquire, 0);
            mAcquiring = 0;
            continue;
        }

        // if the user changes the number of samples to acquire we need
        // to readjust acquisition parameters
        getIntegerParam(P_NumSamples, &numSamples);
        if (mSampleCount != numSamples) {
            // setup the acquisition parameters
            status = setupDevice();
            if (status) {
                setIntegerParam(ADAcquire, 0);
                mAcquiring = 0;
                continue;
            }
            mSampleCount = numSamples;
        }

        this->unlock();

        /* Update the data */
        status = updateArrays();

        getIntegerParam(P_ExtTrigEnabled, &extTrigEnabled);
        if (! extTrigEnabled) {
            sleep(1);
        }

        this->lock();

        if (status) {
            setIntegerParam(ADAcquire, 0);
            mAcquiring = 0;
            continue;
        }

        /* Put the frame number and time stamp into the buffer */
        mUniqueId++;
        getIntegerParam(NDArrayCounter, &arrayCounter);
        arrayCounter++;
        setIntegerParam(NDArrayCounter, arrayCounter);
        epicsTimeGetCurrent(&startTime);

        for (int ch = 0; ch < mChannelCount; ch++) {

            getIntegerParam(ch, P_ChEnabled, &chEnabled);
            if (! chEnabled) {
                continue;
            }
            pTrace = this->pArrays[ch];
            pTrace->uniqueId = mUniqueId;
            pTrace->timeStamp = startTime.secPastEpoch + startTime.nsec / 1.e9;
            updateTimeStamp(&pTrace->epicsTS);
            /* Get any attributes that have been defined for this driver */
            this->getAttributes(pTrace->pAttributeList);
            /* Call the NDArray callback */
            doCallbacksGenericPointer(pTrace, NDArrayData, ch);
            /* Call the callbacks to update any changes */
            callParamCallbacks(ch);
        }
        callParamCallbacks();
    }
}

asynStatus PS4000::setupBuffers() {
    int numSamples;
    PICO_STATUS    status;

    getIntegerParam(P_NumSamples, &numSamples);
    if (mBufferSize != (size_t)numSamples) {
        // free previous buffers
        for (int ch = 0; ch < MAX_CHANNELS; ch++) {
            if (mBuffer[ch]) {
                free(mBuffer[ch]);
                mBuffer[ch] = NULL;
            }
        }

        // allocate new buffers with proper size
        for (int ch = 0; ch < mChannelCount; ch++) {
            mBuffer[ch] = (int16_t *)malloc(numSamples * sizeof(int16_t));
            status = ps4000SetDataBuffer(mHandle, (PS4000_CHANNEL)ch, mBuffer[ch], numSamples);
            if (status) {
                asynPrintDriverInfo(pasynUserSelf, "ps4000SetDataBuffer() returned %d for channel %c",
                    status, 'A' + ch);
            }
        }
        mBufferSize = numSamples;
    }

    return asynSuccess;
}

asynStatus PS4000::setupChannel(int ch) {
    int enabled;
    int dcCoupled;
    int range;
    char name[5] = {0};
    PICO_STATUS    status;

    assert(ch >= PS4000_CHANNEL_A);
    assert(ch <= PS4000_CHANNEL_D);

    getStringParam(ch, P_ChName, 5, name);
    getIntegerParam(ch, P_ChEnabled, &enabled);
    getIntegerParam(ch, P_ChCoupling, &dcCoupled);
    getIntegerParam(ch, P_ChRange, &range);

    asynPrintDriverInfo(pasynUserSelf, "channel %s, enabled %d, coupling %s, range %d",
        name, enabled, (dcCoupled == 0)?"AC":"DC", range);
    // set unit channel parameters
    status = ps4000SetChannel(mHandle, (PS4000_CHANNEL) ch, enabled,
            dcCoupled, (PS4000_RANGE) range);
    asynPrintDriverInfo(pasynUserSelf, "ps4000SetChannel() returned %d", status);
    if (status) {
        return asynError;
    }
    return asynSuccess;
}

asynStatus PS4000::setupExternalTrigger() {
    int enabled;
    int threshold;
    int thresholdMV;
    int direction;
    int range;
    PICO_STATUS status;

    getIntegerParam(P_ExtTrigEnabled, &enabled);
    if (enabled == 0) {
        asynPrintDriverInfo(pasynUserSelf, "disabling external trigger..");
        // disable the trigger
        status = ps4000SetSimpleTrigger(mHandle, 0, PS4000_EXTERNAL, 0,
                ABOVE, 0, 0);
    } else {
        asynPrintDriverInfo(pasynUserSelf, "enabling external trigger..");
        getIntegerParam(P_ExtTrigRange, &range);
        if (range == PS4000_500MV || range == PS4000_5V) {
            status = ps4000SetExtTriggerRange(mHandle, (PS4000_RANGE)range);
            // printf("%s: ps4000SetSimpleTrigger() returned %d\n", __func__, status);
            asynPrintDriverInfo(pasynUserSelf, "ps4000SetSimpleTrigger() returned %d", status);
        }

        getIntegerParam(P_ExtTrigThr, &thresholdMV);
        getIntegerParam(P_ExtTrigThrDir, &direction);
        threshold = mvToAdc(thresholdMV, range);
        asynPrintDriverInfo(pasynUserSelf, "external trigger threshold %d counts, %d mV", threshold, thresholdMV);

        // external trigger will happen once ADC count passes threshold
        status = ps4000SetSimpleTrigger(mHandle, 1, PS4000_EXTERNAL, threshold,
                (THRESHOLD_DIRECTION)direction, 0, 0);
    }
    asynPrintDriverInfo(pasynUserSelf, "ps4000SetSimpleTrigger() returned %d", status);

    if (status) {
        return asynError;
    }
    return asynSuccess;
}

asynStatus PS4000::setupTimeBase() {
    int timeBase;
    int timeInterval;
    int sampleCount;
    int maxSamples;
    PICO_STATUS status;

    getIntegerParam(P_NumSamples, &sampleCount);
    getIntegerParam(P_TimeBase, &timeBase);

    /*
     * Find the maximum number of samples, and the time interval (in nanoseconds),
     * at the current timebase if it is valid.
     * If the timebase index is not valid, increment by 1 and try again.
     */
    while ((status = ps4000GetTimebase(mHandle, (unsigned int)timeBase,
            sampleCount, &timeInterval, 1, &maxSamples, 0))) {
        timeBase++;
        asynPrintDriverInfo(pasynUserSelf, "new timebase %d, samplecount %d, max samples %d, timeInterval %d (status %d)",
            timeBase, sampleCount, maxSamples, timeInterval, status);
        if (status == PICO_INVALID_CHANNEL) {
            printf("%s: at least one channel needs to be enabled!\n", __func__);
            asynPrintError(pasynUserSelf, "at least one channel needs to be enabled");
            setIntegerParam(P_Status, 10);
            setStringParam(P_StatusMessage, "Enable at least one channel!");
            return asynError;
        }
    }
    asynPrintDriverInfo(pasynUserSelf, "ps4000GetTimebase() returned %d", status);
    asynPrintDriverInfo(pasynUserSelf, "final timebase %d, samplecount %d, max samples %d, timeInterval %d (status %d)",
        timeBase, sampleCount, maxSamples, timeInterval, status);

    if (status) {
        return asynError;
    }

    setIntegerParam(P_TimeBase, timeBase);
    setIntegerParam(P_TimeInterval, timeInterval);

    return asynSuccess;
}

/** Called when asyn clients call pasynInt32->write().
  * This function performs actions for some parameters, including ADAcquire, ADColorMode, etc.
  * For all parameters it sets the value in the parameter library and calls any registered callbacks..
  * \param[in] pasynUser pasynUser structure that encodes the reason and address.
  * \param[in] value Value to write. */
asynStatus PS4000::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    int addr;
    asynStatus status = asynSuccess;

    getAddress(pasynUser, &addr);

    /* Set the parameter and readback in the parameter library.  This may be overwritten when we read back the
     * status at the end, but that's OK */
    status = setIntegerParam(addr, function, value);

    if (function == ADAcquire) {
        setAcquire(value);
    } else {
        /* If this parameter belongs to a base class call its method */
        if (function < FIRST_PS4000_PARAM) {
            status = asynNDArrayDriver::writeInt32(pasynUser, value);
        }
    }

    /* Do callbacks so higher layers see any changes */
    callParamCallbacks(addr);

    if (status) {
        asynPrintError(pasynUser, "writeInt32() error, status=%d function=%d, value=%d",
              status, function, value);
    } else {
        asynPrintDriverInfo(pasynUser, "writeInt32() function=%d, value=%d",
              function, value);
    }
    return status;
}

/** Report status of the driver.
  * Prints details about the driver if details>0.
  * It then calls the ADDriver::report() method.
  * \param[in] fp File pointed passed by caller where the output is written to.
  * \param[in] details If >0 then driver details are printed.
  */
void PS4000::report(FILE *fp, int details)
{

    fprintf(fp, "PS4000 %s\n", this->portName);
    if (details > 0) {
        int numSamples, dataType;
        getIntegerParam(P_NumSamples, &numSamples);
        getIntegerParam(NDDataType, &dataType);
        fprintf(fp, "      # samples:   %d\n", numSamples);
        fprintf(fp, "      Data type:   %d\n", dataType);
    }
    /* Invoke the base class method */
    asynNDArrayDriver::report(fp, details);
}

/** Constructor for PS4000; most parameters are simply passed to asynNDArrayDriver::asynNDArrayDriver.
  * After calling the base class constructor this method creates a thread to acquire detector data,
  * and sets reasonable default values for parameters defined in this class.
  * \param[in] portName The name of the asyn port driver to be created.
  * \param[in] numSamples The maximum number of samples to collect.
  * \param[in] dataType The initial data type (NDDataType_t) of the traces that this driver will create.
  * \param[in] maxBuffers The maximum number of NDArray buffers that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited number of buffers.
  * \param[in] maxMemory The maximum amount of memory that the NDArrayPool for this driver is
  *            allowed to allocate. Set this to -1 to allow an unlimited amount of memory.
  * \param[in] priority The thread priority for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  * \param[in] stackSize The stack size for the asyn port driver thread if ASYN_CANBLOCK is set in asynFlags.
  */
PS4000::PS4000(const char *portName, int numSamples, NDDataType_t dataType,
                               int maxBuffers, size_t maxMemory, int priority, int stackSize)

    : asynNDArrayDriver(portName, MAX_CHANNELS, maxBuffers, maxMemory,
               //0, 0, /* No interfaces beyond those set in ADDriver.cpp */
               asynInt32Mask | asynInt32ArrayMask | asynOctetMask | asynGenericPointerMask | asynDrvUserMask,
               asynInt32Mask | asynInt32ArrayMask | asynOctetMask | asynGenericPointerMask,
               ASYN_CANBLOCK | ASYN_MULTIDEVICE, /* asyn flags*/
               1,                                /* autoConnect=1 */
               priority, stackSize),
    mUniqueId(0), mAcquiring(0),
    mHandle(0), mBufferSize(0), mModel(MODEL_NONE), mFirstRange(PS4000_10MV), mLastRange(PS4000_10MV),
    mHasSignalGenerator(false), mHasETS(false), mChannelCount(0), mSampleCount(0)

{
    int status;
    char versionString[20];
    const char *functionName = "PS4000";

    /* Create an EPICS exit handler */
    epicsAtExit(exitHandler, this);

    /* Create the epicsEvents for signaling to the simulate task when acquisition starts and stops */
    this->mStartEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->mStartEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate() failure for start event");
        return;
    }
    this->mStopEventId = epicsEventCreate(epicsEventEmpty);
    if (!this->mStopEventId) {
        asynPrintError(pasynUserSelf, "epicsEventCreate() failure for stop event");
        return;
    }

    createParam(PS4000StatusString,           asynParamInt32, &P_Status);
    createParam(PS4000StatusMessageString,    asynParamOctet, &P_StatusMessage);

    createParam(PS4000TimeBaseString,         asynParamInt32, &P_TimeBase);
    createParam(PS4000TimeIntervalString,     asynParamInt32, &P_TimeInterval);
    createParam(PS4000NumSamplesString,       asynParamInt32, &P_NumSamples);
    createParam(PS4000ExtTrigEnabledString,   asynParamInt32, &P_ExtTrigEnabled);
    createParam(PS4000ExtTrigRangeString,     asynParamInt32, &P_ExtTrigRange);
    createParam(PS4000ExtTrigThrString,       asynParamInt32, &P_ExtTrigThr);
    createParam(PS4000ExtTrigThrDirString,    asynParamInt32, &P_ExtTrigThrDir);

    createParam(PS4000ChNameString,           asynParamOctet, &P_ChName);
    createParam(PS4000ChEnabledString,        asynParamInt32, &P_ChEnabled);
    createParam(PS4000ChCouplingString,       asynParamInt32, &P_ChCoupling);
    createParam(PS4000ChRangeString,          asynParamInt32, &P_ChRange);
    createParam(PS4000ChConvertString,        asynParamInt32, &P_ChConvert);

    epicsSnprintf(versionString, sizeof(versionString), "%d.%d.%d",
                  DRIVER_VERSION, DRIVER_REVISION, DRIVER_MODIFICATION);
    setStringParam(NDDriverVersion, versionString);
    setIntegerParam(P_Status, 0);
    setStringParam(P_StatusMessage, "Idle");

    setIntegerParam(NDDataType, dataType);
    setIntegerParam(P_NumSamples, numSamples);
    char chName[5] = {0};
    for (int i = 0; i < MAX_CHANNELS; i++) {
        chName[0] = 'A' + i;
        setStringParam(i, P_ChName, chName);
        mBuffer[i] = NULL;
    }

    /* Create the thread that updates the images */
    status = (epicsThreadCreate("PS4000Task",
                                epicsThreadPriorityMedium,
                                epicsThreadGetStackSize(epicsThreadStackMedium),
                                (EPICSTHREADFUNC)acqTaskC,
                                this) == NULL);
    if (status) {
        asynPrintError(pasynUserSelf, "epicsThreadCreate() failure for PS4000 task");
        return;
    }

    asynPrintDriverInfo(pasynUserSelf, "PicoScope 4000 Series Driver");
    status = checkForDevice();
}

PS4000::~PS4000() {
    asynPrintDriverInfo(pasynUserSelf, "Shutdown and freeing up memory");

    // XXX: Need to stop the data thread!!!
    this->lock();
    asynPrintDriverInfo(pasynUserSelf, "Data thread is already down");

    ps4000CloseUnit(mHandle);
    for (int ch = 0; ch < MAX_CHANNELS; ch++) {
        if (mBuffer[ch]) {
            free(mBuffer[ch]);
            mBuffer[ch] = NULL;
        }
    }

    this->unlock();
    asynPrintDriverInfo(pasynUserSelf, "Shutdown complete!");
}

/** Configuration command, called directly or from iocsh */
extern "C" int PS4000Config(const char *portName, int numSamples, int dataType,
                                 int maxBuffers, int maxMemory, int priority, int stackSize)
{
    new PS4000(portName, numSamples, (NDDataType_t)dataType,
                    (maxBuffers < 0) ? 0 : maxBuffers,
                    (maxMemory < 0) ? 0 : maxMemory,
                    priority, stackSize);
    return(asynSuccess);
}

/** Code for iocsh registration */
static const iocshArg initArg0 = {"Port name",     iocshArgString};
static const iocshArg initArg1 = {"# samples",     iocshArgInt};
static const iocshArg initArg2 = {"Data type",     iocshArgInt};
static const iocshArg initArg3 = {"maxBuffers",    iocshArgInt};
static const iocshArg initArg4 = {"maxMemory",     iocshArgInt};
static const iocshArg initArg5 = {"priority",      iocshArgInt};
static const iocshArg initArg6 = {"stackSize",     iocshArgInt};
static const iocshArg * const initArgs[] = {&initArg0,
                                            &initArg1,
                                            &initArg2,
                                            &initArg3,
                                            &initArg4,
                                            &initArg5,
                                            &initArg6};
static const iocshFuncDef configPS4000 = {"PS4000Config", 7, initArgs};
static void configPS4000CallFunc(const iocshArgBuf *args)
{
    PS4000Config(args[0].sval, args[1].ival, args[2].ival, args[3].ival,
                         args[4].ival, args[5].ival, args[6].ival);
}

static void PS4000Register(void)
{
    iocshRegister(&configPS4000, configPS4000CallFunc);
}

extern "C" {
epicsExportRegistrar(PS4000Register);
}
